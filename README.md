# SatNOGS Diplexer

Power Amplifier dev board with MMZ09332BT1.
According to simlation in QUCS:

FREQ 380-480MHz
SWR 1.1-1.5
GAIN 35-36dB

the above characteristics agree with the datasheet of MMZ09332BT1.

![PA Dev Board - Report](https://gitlab.com/librespacefoundation/Power_Amplifier_DevBoard/blob/master/report/report.png)

Repository includes all source files for PCB's and qucs simulation files.

## Repository policy

Libre Space Foundation hardware repositories only track source design files. All needed derivative files (e.g. grb etc) 
for production are created per release, packaged in an archive and uploaded linked to a  [release](https://gitlab.com/librespacefoundation/Power_Amplifier_DevBoard).

Each major release (x.0) is branched out of master. Subsequent fixes (minor or not) are commited on those branches and tagged accordingly.

Master branch is most times under active development, so expect things to break. For production ready and previous releases source files check tags and branches.

## Contribute

The main repository lives on [Gitlab](https://gitlab.com/librespacefoundation/Power_Amplifier_DevBoard) and all Merge Request should happen there.

## License

[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202014--2018-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)

Licensed under the [CERN OHLv1.2](LICENSE).