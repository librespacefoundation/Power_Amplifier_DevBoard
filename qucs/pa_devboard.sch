<Qucs Schematic 0.0.19>
<Properties>
  <View=-603,-377,1037,1651,0.808479,0,401>
  <Grid=10,10,1>
  <DataSet=pa_devboard.dat>
  <DataDisplay=pa_devboard.dpl>
  <OpenDisplay=1>
  <Script=pa_devboard.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Eqn Eqn1 1 0 -100 -28 15 0 0 "dBS11=dB(S[1,1])" 1 "dBS12=dB(S[1,2])" 1 "dBS21=dB(S[2,1])" 1 "dBS22=dB(S[2,2])" 1 "yes" 0>
  <Eqn SWR2 1 170 -100 -28 15 0 0 "absS22=abs(S[2,2])" 1 "swr2=(1+absS22)/(1-absS22)" 1 "yes" 0>
  <Eqn SWR1 1 0 30 -28 15 0 0 "absS11=abs(S[1,1])" 1 "swr1=(1+absS11)/(1-absS11)" 1 "yes" 0>
  <GND * 1 -360 370 0 0 0 0>
  <Pac RFIN 1 -360 320 -94 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <SPfile PA1 1 -70 270 -26 -59 0 0 "/home/azisi/Documents/pqws/pqws-main-hw/qucs/models/MMZ09332B_SP.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <GND * 1 -70 370 0 0 0 0>
  <GND * 1 120 380 0 0 0 0>
  <SUBST FR4 1 -190 -90 -30 24 0 0 "4.5" 1 "1.6mm" 1 "35 um" 1 "0.022" 1 "1.72e-8" 1 "0" 1>
  <SPfile C3 1 180 330 40 -26 0 3 "/home/azisi/Documents/pqws/pa_devboard/qucs/s-parameters/GCM1885C1H180JA16_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C2 1 20 270 -26 -59 0 0 "/home/azisi/Documents/pqws/pa_devboard/qucs/s-parameters/GCM1885C2A221JA16_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L1 1 -180 270 -26 -59 0 0 "/home/azisi/Documents/pqws/pa_devboard/qucs/s-parameters/LQG18HN3N9S00_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L4 1 70 330 -58 -26 0 1 "/home/azisi/Documents/pqws/pa_devboard/qucs/s-parameters/LQW18AN22NG00_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L5 1 120 270 -26 -59 0 0 "/home/azisi/Documents/pqws/pa_devboard/qucs/s-parameters/LQG18HN5N6S00_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <.SP SP1 1 -420 -120 0 76 0 0 "log" 1 "90MHz" 1 "1000MHz" 1 "100000" 1 "no" 0 "2" 0 "1" 0 "yes" 0 "yes" 0>
  <GND * 1 520 350 0 0 0 0>
  <Pac RFOUT 1 520 310 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 350 370 0 0 0 0>
  <SPfile C4 0 390 330 40 -26 0 3 "/home/azisi/Documents/pqws/pa_devboard/qucs/s-parameters/GCM1885C2A120JA16_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C33 0 310 330 -60 -26 0 1 "/home/azisi/Documents/pqws/pa_devboard/qucs/s-parameters/GCM1885C2A2R2BA16_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <CLIN CL4 1 -280 270 -26 -123 1 0 "FR4" 1 "1.0mm" 1 "0.2 mm" 1 "10 mm" 1 "Metal" 0 "yes" 0>
  <CLIN CL2 1 460 270 -26 -123 1 0 "FR4" 1 "1.0 mm" 1 "0.2 mm" 1 "10 mm" 1 "Metal" 0 "yes" 0>
</Components>
<Wires>
  <-360 350 -360 370 "" 0 0 0 "">
  <-360 270 -360 290 "" 0 0 0 "">
  <-360 270 -310 270 "" 0 0 0 "">
  <-40 270 -10 270 "" 0 0 0 "">
  <50 270 70 270 "" 0 0 0 "">
  <70 270 70 300 "" 0 0 0 "">
  <70 360 70 370 "" 0 0 0 "">
  <70 270 90 270 "" 0 0 0 "">
  <150 270 180 270 "" 0 0 0 "">
  <180 270 180 300 "" 0 0 0 "">
  <180 360 180 370 "" 0 0 0 "">
  <70 370 120 370 "" 0 0 0 "">
  <120 370 180 370 "" 0 0 0 "">
  <120 370 120 380 "" 0 0 0 "">
  <100 330 120 330 "" 0 0 0 "">
  <120 300 120 330 "" 0 0 0 "">
  <120 330 120 370 "" 0 0 0 "">
  <20 370 70 370 "" 0 0 0 "">
  <20 300 20 370 "" 0 0 0 "">
  <120 330 150 330 "" 0 0 0 "">
  <-150 270 -100 270 "" 0 0 0 "">
  <-250 270 -210 270 "" 0 0 0 "">
  <-180 300 -180 360 "" 0 0 0 "">
  <-70 300 -70 360 "" 0 0 0 "">
  <-70 360 -70 370 "" 0 0 0 "">
  <-180 360 -70 360 "" 0 0 0 "">
  <180 270 310 270 "" 0 0 0 "">
  <520 340 520 350 "" 0 0 0 "">
  <520 270 520 280 "" 0 0 0 "">
  <490 270 520 270 "" 0 0 0 "">
  <350 330 350 370 "" 0 0 0 "">
  <310 270 390 270 "" 0 0 0 "">
  <390 270 430 270 "" 0 0 0 "">
  <340 330 350 330 "" 0 0 0 "">
  <310 270 310 300 "" 0 0 0 "">
  <310 370 350 370 "" 0 0 0 "">
  <310 360 310 370 "" 0 0 0 "">
  <350 330 360 330 "" 0 0 0 "">
  <350 370 390 370 "" 0 0 0 "">
  <390 360 390 370 "" 0 0 0 "">
  <390 270 390 300 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect -450 1263 1440 353 3 #c0c0c0 1 00 1 3e+08 1e+07 6e+08 1 -73.1119 20 46.3153 1 -1 0.5 1 315 0 225 "" "" "">
	<"dBS11" #ff0000 0 3 0 0 0>
	<"dBS12" #000000 0 3 0 0 0>
	<"dBS21" #00ff00 0 3 0 0 0>
	  <Mkr 4.33233e+08 470 -262 3 0 0>
	  <Mkr 3.90497e+08 226 -290 3 0 0>
	  <Mkr 4.96659e+08 724 -292 3 0 0>
	<"dBS22" #0000ff 0 3 0 0 0>
  </Rect>
  <Rect -440 847 1420 347 3 #c0c0c0 1 00 1 3e+08 1e+07 6e+08 1 1 0.2 2.64606 1 -1 0.5 1 315 0 225 "" "" "">
	<"swr1" #0000ff 0 3 0 0 0>
	  <Mkr 4.33201e+08 751 -284 3 0 0>
	  <Mkr 4.80599e+08 710 -150 3 0 0>
	  <Mkr 3.97929e+08 541 -280 3 0 0>
	  <Mkr 3.78708e+08 251 -261 3 0 0>
	  <Mkr 3.72297e+08 181 -161 3 0 0>
	<"swr2" #ff0000 0 3 0 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Text 420 -100 12 #000000 0 "FR4 Substrace\nhttp://www.elecrow.com/wiki/images/3/34/PCB_material_FR-4_info.pdf\nhttps://www.elecrow.com/wiki/index.php?title=Q%26A_for_PCB_service\n">
  <Text 420 -40 12 #000000 0 "C2, GCM1885C2A221JA16, 220pF\nL5, LQG18HN5N6S00, 5.6nH\nL4, LQW18AN22NG00, 22nH\nC3, GCM1885C1H180JA16, 18pF\nL1, LQG18HN3N9S00D, 3.9nH\nGCM1885C2A120JA16, 12pF\nGCM1885C2A2R2BA16, 2.2pF\n\n">
</Paintings>
